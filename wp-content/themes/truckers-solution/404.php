<?php
get_header()
?>

<div id="main-content">

    <div class="entry-content">
        <div id="et-boc" class="et-boc">

            <div class="et_builder_inner_content et_pb_gutters3">
                <div class="et_pb_section et_pb_section_0 padding-top-3x padding-bottom-3x overlap-bottom et_pb_with_background et_section_regular dsm-404-content">

                    <div class="et_pb_row et_pb_row_0">
                        <div class="et_pb_column et_pb_column_4_4 et_pb_column_0    et_pb_css_mix_blend_mode_passthrough et-last-child">


                            <div class="et_pb_module et_pb_text et_pb_text_0 et_pb_bg_layout_dark  et_pb_text_align_center">


                            <div class="et_pb_text_inner">
                            <h1 class="white dsm-hero-title align-center">OH NO! ERROR 404</h1>
                            </div>
                            </div> <!-- .et_pb_text -->
                            <div class="et_pb_module et_pb_text et_pb_text_1 et_pb_bg_layout_light  et_pb_text_align_left">


                                <div class="et_pb_text_inner">
                                    <h3 class="subsection-header white align-center">IT LOOKS LIKE YOU’VE VEERED OFF THE PATH.</h3>
                                </div>
                            </div> <!-- .et_pb_text -->

                            <div class="et_pb_module et_pb_text et_pb_text_2 et_pb_bg_layout_light  et_pb_text_align_left">
                                <div class="et_pb_text_inner">
                                    <p class="light-grey align-center">The page you’re looking for cannot be found or no longer exists. But don’t worry! We’ll get you back on the right path.</p>
                                </div>
                            </div> <!-- .et_pb_text -->
                            <div class="et_pb_button_module_wrapper et_pb_button_0_wrapper et_pb_button_alignment_center et_pb_module ">
                                <a class="et_pb_button et_pb_button_0 dsm-btn dsm-btn-secondary dsm-btn-red dsm-light-text et_hover_enabled et_pb_bg_layout_dark" href="<?php echo $_SERVER['HTTP_REFERRER'];?>">RETURN TO PREVIOUS PAGE</a>
                            </div>
                        </div> <!-- .et_pb_column -->


                    </div> <!-- .et_pb_row -->


                </div> <!-- .et_pb_section -->
                <div class="et_pb_section et_pb_section_2 no-pad-bottom no-pad-top dsm-content-bottom et_pb_equal_columns et_pb_specialty_fullwidth et_pb_with_background et_section_specialty section_has_divider et_pb_bottom_divider">



                    <div class="et_pb_row et_pb_gutters1">
                        <div class="et_pb_column et_pb_column_1_2 et_pb_column_1  dsm-content-bottom-col-left  et_pb_css_mix_blend_mode_passthrough et_pb_column_single">


                            <div class="et_pb_module et_pb_text et_pb_text_3 et_pb_bg_layout_light  et_pb_text_align_left">


                                <div class="et_pb_text_inner">
                                    <h3 class="white footer-header">YOU DON'T WORK NORMAL HOURS, SO WHY SHOULD WE?</h3>
                                </div>
                            </div> <!-- .et_pb_text -->
                            <div class="et_pb_module et_pb_text et_pb_text_4 et_pb_bg_layout_light  et_pb_text_align_left">


                                <div class="et_pb_text_inner">
                                    <h4 class="white footer-subheader">HAVE AN URGENT NEED?</h4>
                                </div>
                            </div> <!-- .et_pb_text -->
                            <div class="et_pb_module et_pb_text et_pb_text_5 et_pb_bg_layout_light  et_pb_text_align_left">

                                <div class="et_pb_text_inner">
                                    <p class="footer-text">Our after-hours coverage means you can still get a real human at any time you need it. Goodbye, frustration. Hello, better solutions.</p>
                                </div>
                            </div> <!-- .et_pb_text -->
                            <div class="et_pb_button_module_wrapper et_pb_button_1_wrapper et_pb_button_alignment_left et_pb_module ">
                                <a class="et_pb_button et_pb_custom_button_icon et_pb_button_1 dsm-hero-btn dsm-btn dsm-btn-red dsm-btn-primary dsm-dark-text et_hover_enabled et_pb_bg_layout_light" href="/contact/" data-icon="$" style="">CONTACT US</a>
                            </div>
                        </div> <!-- .et_pb_column -->
                        <div class="et_pb_column et_pb_column_1_2 et_pb_column_2  dsm-content-bottom-col-right et_pb_specialty_column  et_pb_css_mix_blend_mode_passthrough">


                            <div class="et_pb_row_inner et_pb_row_inner_0 no-pad-bottom">
                                <div class="et_pb_column et_pb_column_4_4 et_pb_column_inner et_pb_column_inner_0   et-last-child">


                                    <div class="et_pb_module et_pb_text et_pb_text_6 et_pb_bg_layout_dark  et_pb_text_align_left">


                                        <div class="et_pb_text_inner">
                                            <h2 class="section-header">Get In Touch</h2>
                                        </div>
                                    </div> <!-- .et_pb_text -->
                                    <div class="et_pb_module et_pb_text et_pb_text_7 et_pb_bg_layout_dark  et_pb_text_align_left">


                                        <div class="et_pb_text_inner">
                                            <a href="tel:+1-352-241-6000" class="footer-phone">(352) 241-6000</a>
                                        </div>
                                    </div> <!-- .et_pb_text -->
                                </div> <!-- .et_pb_column -->


                            </div> <!-- .et_pb_row_inner -->
                            <div class="et_pb_row_inner et_pb_row_inner_1 padding-top-half et_pb_row_1-4_1-4">
                                <div class="et_pb_column et_pb_column_1_4 et_pb_column_inner et_pb_column_inner_1  ">


                                    <div class="et_pb_module et_pb_text et_pb_text_8 et_pb_bg_layout_dark  et_pb_text_align_left">


                                        <div class="et_pb_text_inner">
                                            <h4>LOCATION</h4>
                                            <p>504 Autumn Springs Ct C22,<br>
                                            Franklin, TN 37067
                                            </p>
                                        </div>
                                    </div> <!-- .et_pb_text -->
                                    <div class="et_pb_module et_pb_text et_pb_text_9 et_pb_bg_layout_dark  et_pb_text_align_left">


                                        <div class="et_pb_text_inner">
                                            <h4>CONTACT</h4>
                                            <p><a href="mailto:info@truckerssolution.com">info@truckerssolution.com</a><br>
                                            Fax: 352-241-4177
                                            </p>
                                        </div>
                                    </div> <!-- .et_pb_text -->
                                </div> <!-- .et_pb_column -->

                                <div class="et_pb_column et_pb_column_1_4 et_pb_column_inner et_pb_column_inner_2  ">
                                    <div class="et_pb_module et_pb_text et_pb_text_10 et_pb_bg_layout_dark  et_pb_text_align_left">


                                        <div class="et_pb_text_inner">
                                            <h4>HOURS</h4>
                                            <p>Mon—Fri: 7am – 6pm<br>
                                            Sat: 8am – 4pm
                                            </p>
                                        </div>
                                    </div> <!-- .et_pb_text -->
                                    <div class="et_pb_module et_pb_text et_pb_text_11 et_pb_bg_layout_light  et_pb_text_align_left">


                                        <div class="et_pb_text_inner">
                                            <h4 class="white">LET'S CONNECT</h4>
                                            <div class="footer-social dsm-social-wrapper"><a href="#"><i class="fab fa-2x fa-facebook-f"></i></a><a href="#"><i class="fab fa-2x fa-instagram"></i></a><a href="#"><i class="fab fa-2x fa-twitter"></i></a></div>
                                        </div>
                                    </div> <!-- .et_pb_text -->
                                </div> <!-- .et_pb_column -->

                            </div> <!-- .et_pb_row_inner -->
                        </div> <!-- .et_pb_column -->
                    </div> <!-- .et_pb_row -->
                    <div class="et_pb_bottom_inside_divider"></div>
                </div> <!-- .et_pb_section -->
            </div>
        </div>
    </div> <!-- .entry-content -->
</div>

<?php get_footer();
